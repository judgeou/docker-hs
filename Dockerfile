FROM node:alpine

RUN npm config set registry https://registry.npm.taobao.org/
RUN npm install -g http-server

WORKDIR /hs

CMD ["hs"]

